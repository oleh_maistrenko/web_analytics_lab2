const csv = require('csv-parser');
const fs = require('fs');

(async () => {
    const data = [];
    await parseCsv(data);

    const amountMax = Math.max.apply(Math, data.map((obj) => obj.amount ));
    normalizeAmount(data, amountMax);


    const data1 = data.filter((data) => data.category === "Трафик планшетных и обычных ПК");
    const data2 = data.filter((data) => data.category === "Неоднократные посетители");
    const data3 = data.filter((data) => data.category === "Трафик с мобильных устройств");

    data1.sort((a, b) => a.amount - b.amount);
    data2.sort((a, b) => a.amount - b.amount);
    data3.sort((a, b) => a.amount - b.amount);

    const anomalies1 = detectAnomalies(data1);
    const anomalies2 = detectAnomalies(data2);
    const anomalies3 = detectAnomalies(data3);

    fs.writeFileSync('./results1', JSON.stringify(anomalies1, null, 2));
    fs.writeFileSync('./results2', JSON.stringify(anomalies2, null, 2));
    fs.writeFileSync('./results3', JSON.stringify(anomalies3, null, 2));
})();

function parseCsv(results) {
    return new Promise((resolve, reject) => {
        fs.createReadStream('analytics.csv')
        .pipe(csv(['time', 'date', 'category', 'amount']))
        .on('data', (data) => results.push(data))
        .on('end', () => {
            resolve(results);
        });
    });
}
function normalizeAmount(results, amountMax) {
    for (let i = 0; i < results.length; i++) {
        results[i].amount /= amountMax;
    }
}
function groupBy(list, keyGetter) {
    const map = new Map();
    list.forEach((item) => {
        const key = keyGetter(item);
        const collection = map.get(key);
        if (!collection) {
            map.set(key, [item]);
        } else {
            collection.push(item);
        }
    });
    return map;
}
function getMedian(data) {
    let lowMiddle = Math.floor((data.length - 1) / 2);
    let highMiddle = Math.ceil((data.length - 1) / 2);
    return (data[lowMiddle].amount + data[highMiddle].amount) / 2;
}
function getQ1(data) {
    let lowMiddle = Math.floor((data.length - 1) / 4);
    let highMiddle = Math.ceil((data.length - 1) / 4);
    return (data[lowMiddle].amount + data[highMiddle].amount) / 2;
}
function getQ3(data) {
    let lowMiddle = Math.floor((data.length - 1) / 4 * 3);
    let highMiddle = Math.ceil((data.length - 1) / 4 * 3);
    return (data[lowMiddle].amount + data[highMiddle].amount) / 2;
}
function detectAnomalies(data) {
    const anomalies = [];
    const M = getMedian(data);
    const Q3 = getQ3(data);
    const Q1 = getQ1(data);
    for (const [key, value] of Object.entries(data)) {
        if (Math.abs(value.amount - M) > 1.5 * (Q3 - Q1)) {
            anomalies.push({
                category: value.category,
                date: convertTime(value.time, value.date),
                amount: value.amount,
            })
        }
    }

    return anomalies;
}
function convertTime(hoursShift, startDate) {
    const val = startDate.slice(0, 10).split('.')
    const start = new Date(`${val[2]}-${val[1]}-${val[0]}`);
    start.setHours(start.getHours() + +hoursShift);
    return start.toGMTString();
}
